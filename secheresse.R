
library(lubridate)
library(tidyverse)
library(RCurl)
library(curl)
library(datalibaba)
library(dplyr)
library(R.utils)
library(remotes)
library(sf)
#library(RPostgres)
library(readODS)
library(tricky)
library(googlesheets4)
library(archive)
#library(tidyquery)

Sys.setenv(PROJ_LIB="")
options(timeout = max(300, getOption("timeout")))

#récupération des sources
#zones restriction sécheresse

url <- "https://www.data.gouv.fr/fr/datasets/r/bfba7898-aed3-40ec-aa74-abb73b92a363"

destfile <-"data/zones-arretes-en-vigueur.geojson"
download.file(url, destfile, mode="wb")
#gunzip(destfile,"data/zones-arretes-en-vigueur.geojson", overwrite = TRUE )
zonesrestrictions <- st_read("data/zones-arretes-en-vigueur.geojson") %>%
  st_make_valid() %>%
   filter(str_detect(departement, "44|49|53|72|85"))

# # Récupération référentiel régional 
# region <- importer_data(table = 'n_region_exp_r52', schema = 'adminexpress', db = 'referentiels') %>%
#   st_set_crs(2154)   %>%
#   st_transform(crs=4326)
# 
# # jointure restriction et region pdl
# zonesrestrictions <- st_intersection(zonesrestrictionsfrance, region)
# 
# poster_data(data=zonesrestrictions, table = "n_zones_restrictions_secheresse_s_r52", schema = "zonages_techniques_reglementaires", db = "si_eau", user = "admin", pk = "id", overwrite = TRUE)




# récupération géométrie des zones alertes secheresse

# url2 <- "https://www.data.gouv.fr/fr/datasets/r/f1076fa8-04fd-4cde-9544-bf7a1f5ad36f"
# destfile2 <- "data/all_zones.shp.zip"
# download.file(url2, destfile2, mode="wb")
# unzip(zipfile = destfile2, exdir = "data",  overwrite = TRUE)
# zonesalertessecheresse <- st_read("data/all_zones.shp") %>%
#   st_make_valid() %>%
#   filter(code_dep %in% c("44", "49", "53", "72", "85"))
# zonesalertessecheresse1 <- zonesalertessecheresse   %>%
#   filter(est_max_v == 1) %>%
#   arrange(desc(n_version)) %>%
#   distinct(code_zone, type_zone, code_dep, .keep_all = TRUE) %>%
#   select(id_zone, code_zone, type_zone, nom_zone)
#
  

# Lire les zones secheresse AEP

#zas_aep_44 <- importer_data(table = 'n_zone_alerte_secheresse_aep_s_044', schema = 'zonages_techniques_reglementaires', db = 'si_eau') %>%
#   st_set_crs(2154)   %>%
#   st_transform(crs=4326) %>%
#   st_make_valid()
# zas_aep_49 <- importer_data(table = 'n_zone_alerte_secheresse_aep_s_049', schema = 'zonages_techniques_reglementaires', db = 'si_eau') %>%
#   st_set_crs(2154)   %>%
#   st_transform(crs=4326) %>%
#   st_make_valid()
# zas_aep_r52 <- bind_rows(zas_aep_49, zas_aep_44) %>%
#   rename (nom_zone = libelle_zone) %>%
#   rename (code_zone = id_zone) %>%
#   rename (geometry = the_geom) %>%
#   mutate(type_zone = 'AEP') %>%
#   select (-("departement"))
#r rm(zas_aep_44, zas_aep_49)
# 
# # Combiner les zones secheresse
# 
# zonealertesecheresse1 <- bind_rows(zonealertesecheresse, zas_aep_r52)
# poster_data(data=zonealertesecheresse1, table = "n_zone_alerte_secheresse_r52", 
#                         schema = "zonages_techniques_reglementaires", db = "si_eau", user = "admin", 
#                       overwrite = TRUE)
  

# Récupérer zones complètes alerte secheresse

zonealertesecheresse1 <- importer_data(table = 'n_zone_alerte_secheresse_r52', schema = 'zonages_techniques_reglementaires', db = 'si_eau')

#liste_icpe 
role <- "admin"
icpe <- importer_data("consultation", schema = "site_industriel_production", table = "n_icpe_dreal_p_r52") %>%
  st_set_crs(2154)   %>%
st_transform(crs=4326) %>%
  filter (regimevigueur %in% c("Autorisation", "Enregistrement") ) %>%
  filter (etatactivite != "En fin d'exploitatition") %>%
  filter (raisonsociale != "test") %>%
  select(codeaiot, raisonsociale, commune, codeinsee, regimevigueur, serviceaiot, rubrique, the_geom)

#liste enquête acs

dossier_acs <- read_ods("T:/datalab/SRNT/SRNT_DRC/SECHERESSE/DONNEES_CLIENT/DS_nettoye.ods", sheet = "Dossiers") %>%
  rename_with(~gsub("’", "'", .x))  %>%
   select(codeaiot = 'Code Inspection AIOT',
          psup_10000 = 'Prélevez-vous annuellement une quantité d\'eau (tous milieux : adduction eau potable, prélèvement en eaux superficielles, prélèvement en eaux souterraines, etc.) supérieure à 10 000 mètres cubes ?', 
          pautorise_sup_10000 = 'Etes-vous autorisés à prélever annuellement une quantité d\'eau (tous milieux : adduction eau potable, prélèvement en eaux superficielles, prélèvement en eaux souterraines, etc.) supérieure à 10 000 mètres cubes ?',
          p_aep = 'Prélèvement dans le réseau d\'adduction en eau potable', 
          vol_aep = 'Part du volume de référence prélevée dans le réseau d\'adduction en eau potable (m3/jour)', 
          p_esu = 'Prélèvement dans les eaux superficielles ou dans une nappe d\'accompagnement d\'un cours d\'eau', 
          vol_esu = 'Part du volume de référence prélevée dans les eaux superficielles ou nappes d\'accompagnement (en m3/jour)', 
          p_eso = 'Prélèvement dans les eaux souterraines', 
          vol_eso = 'Part du volume de référence prélevée dans les eaux souterraines (m3/jour)', 
          cap_aep = 'Captage, traitement et distribution d\'eau destinée à la consommation humaine (eau potable) ou d\'eaux conditionnées (eau de source, eau rendue potable par traitements, eau minérale naturelle)', 
          cap_sante = 'Captage, traitement et distribution d\'eau destinée aux établissements de santé, aux établissements et aux services sociaux et médico-sociaux', 
          eau_animal = 'Alimentation en eau pour l\'abreuvement, la santé, la survie et le bien-être des animaux et le respect des règles sanitaires liées aux animaux', 
        #  transfo_agro = ' Transformation agroalimentaire en flux poussé ', 
          prod_cogene = 'Production, distribution et cogénération d\'électricité', 
          prod_enr = 'Production et distribution d\'énergie produite à partir de sources renouvelables mentionnées à l\'article L. 211-2 du code de l\'énergie', 
          prod_medoc = 'Production de médicaments d\'intérêt thérapeutique majeur et leurs principes actifs ou de médicaments contribuant à une politique de santé publique définie par le ministre chargé de la santé', 
          collecte_tri= 'Collecte, tri, transit, regroupement et traitement de déchets dangereux et non dangereux', 
          nettoyage_textile = 'Nettoyage des textiles utilisés au sein d\'établissements de santé', 
          reduc_p_eau_20pc = 'Avez vous réduit vos prélèvement d\'eau d\'au moins 20 % depuis le 1er janvier 2018 ?', 
          reutilisation_eau_20pc = 'Utilisez-vous au moins 20 % d\'eaux réutilisées par rapport à vos prélèvements d\'eau, sous réserve du respect des exigences sanitaires et environnementales en vigueur')




#jointure enquête acs et icpe

icpe_acs <- left_join(icpe, dossier_acs, by = c("codeaiot" = "codeaiot")) %>%
  filter (raisonsociale != "") %>%
  mutate(psup_10000 = ifelse(is.na(psup_10000),"NSP", psup_10000)) %>%
  mutate(pautorise_sup_10000 = ifelse(is.na(pautorise_sup_10000),"NSP", pautorise_sup_10000)) %>%
 # mutate(p_aep = ifelse(is.na(p_aep),"NSP", p_aep)) %>%
 # mutate(p_esu = ifelse(is.na(p_esu),"NSP", p_esu)) %>%
 # mutate(p_eso = ifelse(is.na(p_eso),"NSP", p_eso)) %>%
  mutate(vol_aep = ifelse(is.na(vol_aep),0, vol_aep)) %>%
  mutate(vol_esu = ifelse(is.na(vol_esu),0, vol_esu)) %>%
  mutate(vol_eso = ifelse(is.na(vol_eso),0, vol_eso)) %>%
  mutate_all(~replace_na(.,"NSP")) 
  poster_data(data = icpe_acs, table = "n_icpe_enquete_acs_r52",
              schema = "zonages_techniques_reglementaires",
              db = "si_eau", 
              user = "admin",
              overwrite = TRUE)
  
# doudlons crees
doublons_acs <- dossier_acs %>% 
  count(codeaiot) %>% 
  filter(n>1) %>%
  pull(codeaiot)

doublons_acs

#jointure spatiale icpe_acs et zas
join_icpe_zas <- st_join(icpe_acs, zonealertesecheresse1) %>%
  pivot_wider(id_cols = c(codeaiot:reutilisation_eau_20pc, the_geom),
              names_from = type_zone, values_from = code_zone:nom_zone, names_glue = "{type_zone}_{.value}") %>%
  mutate (sou_zone = paste(SOU_code_zone, SOU_nom_zone), sup_zone = paste(SUP_code_zone, SUP_nom_zone), aep_zone = paste(AEP_code_zone, AEP_nom_zone)) %>%
select (-starts_with("NA_"), -("SOU_code_zone"), -("SOU_nom_zone"), -("SUP_code_zone"), -("SUP_nom_zone"), -("SOU_type_zone"), -("SUP_type_zone"),
        -("AEP_nom_zone"), -("AEP_type_zone"), -("AEP_code_zone") ) 

#jointure spatiale join_icpe_zas et zonesrestrictions
icpe_zas_restriction <- st_join(join_icpe_zas, zonesrestrictions) %>%
  mutate(restriction_type = type, restriction_gravite = niveauGravite) %>%
select( (-"id"), -("nom"), -("code"), -("departement"), -("arreteRestriction"), 
        -("restrictions"), -("type"), -("niveauGravite")) %>%
  st_drop_geometry()
  




#Export fichier csv
write.csv( icpe_zas_restriction, file = "T:/datalab/SRNT/SRNT_DRC/SECHERESSE/LIVRABLE/croisement_icpe_zas.csv")


# Mise à jour du fichier googlesheet

gs4_auth_configure(api_key = Sys.getenv("google_api_key"))
gs4_auth()
sheet_write("https://docs.google.com/spreadsheets/d/1bgEIPVBQWlgef_7rsmM27qU8FeIt_TWXHJZerPdTPVM/edit?gid=0#gid=0", data=icpe_zas_restriction, sheet = "Feuille 1")
